package org.tastefuljava.kdbx;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class KbdxReader implements Closeable {
    private final InputStream in;

    public static KbdxReader open(File file) throws IOException {
        return new KbdxReader(new FileInputStream(file));
    }

    public static KbdxReader open(String fileName) throws IOException {
        return new KbdxReader(new FileInputStream(fileName));
    }

    private KbdxReader(InputStream in) {
        this.in = in;
    }

    @Override
    public void close() throws IOException {
        in.close();
    }

    public FileHeader readHeader() throws IOException {
        return FileHeader.readFrom(in);
    }
}
