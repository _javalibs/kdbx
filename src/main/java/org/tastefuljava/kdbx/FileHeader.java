package org.tastefuljava.kdbx;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class FileHeader {
    private final long magic;
    private final int version;
    private final HeaderItem[] items;

    public static FileHeader readFrom(InputStream in) throws IOException {
        byte[] bytes = readBytes(in, 15); // file header plus 3 bytes of first
        ByteBuffer buf = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
        long magic = buf.getLong();
        int version = buf.getInt();
        // Headers
        List<HeaderItem> items = new ArrayList<>();
        while (true) {
            byte type = buf.get();
            int length = buf.getShort() & 0xFFFF;
            if (type == 0) {
                // last header
                bytes = readBytes(in, length);
                items.add(new HeaderItem(type, bytes));
                break;
            }
            bytes = readBytes(in, length+3);
            items.add(new HeaderItem(type, Arrays.copyOf(bytes, length)));
            buf = ByteBuffer.wrap(bytes, length, 3)
                    .order(ByteOrder.LITTLE_ENDIAN);
        }
        return new FileHeader(
                magic, version, items.toArray(new HeaderItem[items.size()]));
    }

    private static byte[] readBytes(InputStream in, int length)
            throws IOException {
        byte[] bytes = new byte[length];
        if (length > 0) {
            if (in.read(bytes) != bytes.length) {
                throw new IOException("Unexpected end of file");
            }
        }
        return bytes;
    }

    private FileHeader(long magic, int version, HeaderItem[] items) {
        this.magic = magic;
        this.version = version;
        this.items = items;
    }

    public long getMagic() {
        return magic;
    }

    public int getVersion() {
        return version;
    }

    public void forEachItem(Consumer<HeaderItem> cons) {
        for (HeaderItem item: items) {
            cons.accept(item);
        }
    }
}
