package org.tastefuljava.kdbx;

public class HeaderItem {
    private final byte type;
    private final byte[] value;

    HeaderItem(byte type, byte[] value) {
        this.type = type;
        this.value = value;
    }

    public byte getType() {
        return type;
    }

    public byte[] getValue() {
        return value;
    }
}
