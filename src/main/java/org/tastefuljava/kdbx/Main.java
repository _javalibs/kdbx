package org.tastefuljava.kdbx;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        try (KbdxReader reader = KbdxReader.open("Database.kdbx")) {
            FileHeader header = reader.readHeader();
            System.out.println(Long.toUnsignedString(header.getMagic(), 16));
            System.out.println(
                    Integer.toUnsignedString(header.getVersion(), 16));
            header.forEachItem((h)->{
                System.out.println("Header");
                System.out.println("    Type: " + h.getType());
                System.out.println("    Length: " + h.getValue().length);
            });
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
